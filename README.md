
# For Asknicely Test
[TOC]

# Background of the project
A simple employee information management tool, meanwhile a test project for technical interviews.
## Core Functions
 - Upload employee information via CSV;
 - Display imported employee information in list;
 - Modify employee email;
 - Display average salary information for each company;

# Quick start
The project provides services in the form of a web application, which can be directly experienced and used by visiting my page https://richardw.site/ (deployed on a cheap 1core+1G cloud service computer, the machine may not be stable). Or deploy the service via Docker wherever you like, please follow the specific steps below to install and start the project.

## How to start
- Clone the repository to your local environment
```
$ git clone https://gitlab.com/wangyuqi8080/asknicely_test.git
```
- CD in project repository
```
$ cd ./asknicely_test
```
- Use docker-compose to start the service in the current directory. Note: If the host's ports 80 and 3306 are occupied, remember to kill the corresponding processes and then start them again.
```
$ docker-compose up -d
```
![Alt text](image-1.png)
- Check whether the container started successfully. When you see the same return as the picture below, congratulations, the project started successfully!
```
$ docker-compose ps
```
![Alt text](image.png)
- Finally, access your hostname or local 127.0.0.1 directly in the browser to start using the system!

## Simple guide
![Alt text](image-4.png)

# Ideas and implementation

The core requirement of the project is to implement a simple file upload and data display management function, and to examine the developer's coding capabilities in PHP and JS through the project. At the same time, the testing time is limited, and the project is not required to be perfect in terms of architecture, service governance, system security, disaster recovery capabilities, scalability, etc. More complete plans and details can be reflected in this document.

Therefore, it is assumed that the project is currently an internal management system with a small number of users, low read and write QPS, and no requirements for multiple regions and high concurrent access.

Technology selection and coding principles:
- Meet the interview question requirements;
- Simple and usable;
- While reflecting sufficient coding capabilities, this simple version can lead to more and more complete details and thinking;
- Save costs in deployment;

## Architecture
Based on the above principles and assumptions：
- The front-end is used for independent rendering, and the back-end provides reading and writing capabilities to the front-end through HTTP API.
- Storage only relies on mysql as RDB to provide storage and query capabilities, and has no other storage services.
- The code deployment method is localized single mode. Although the front-end and back-end are isolated from the development and application dimensions through the API protocol, the deployment in the same container and the same nginx process is essentially a single service.
- A separate mysql image is used for storage, but the mysqld process and the nginx process are on the same host instance, so the entire service is still a single entity. <br/>
Arch diagram:
![Alt text](systemArch.png)

## Selection
- Web Server <br/>
    Nginx <br/>
    Reason:<br/>
    - Lighter and takes up less resources
    - Communicates with php-fpm through cgi protocol, friendly to php support
    - The configuration method is simple. Mount the corresponding html and php scripts to the corresponding directories to achieve front-end and back-end separation.
- Backend framework <br/>
    YAF <br/>
    Reason:<br/>
    - Simple and concise enough, except for the core route dispatching, autoloaded mechanism, there are no other redundant business class libraries. The entire project structure needs to be built from scratch, which is very suitable for the development scenario of this interview project.
    - Written in C language and provided as a PHP extension. Compared with other frameworks written in PHP, the running cost of the framework itself is lower.
    - Included in the official pecl library (https://pecl.php.net/package/yaf), installation and compilation are convenient and simple.
    - Very flexible, with enough space to define and develop various class libraries suitable for your own business.
    - I have used the non-open source version of this framework while working internally at Baidu and am relatively familiar with it.
- Front-end framework <br/>
    React+antd Lib <br/>
    Reason: <br/>
    - Virtual DOM feature performance is relatively better.
    - The antd (https://ant.design/) library supports a wide range of component types and can quickly complete this written test function. Especially for me who is not particularly familiar with js.
- Deployment <br/>
    Docker-compose <br/>
    Reason: <br/>
    - Containerized deployment supports running on different platforms.
    - Easy to start and use, friendly for developers and interviewers to run and review functions.
    - The community is friendly, and it is very convenient to have dockerhub to host customized images (this nginx-php image based on ubuntu: https://hub.docker.com/r/richardwong666/nginx-php for this project).
- Storage <br/>
   Mysql <br/>
   Reason: <br/>
   - Business scenarios require RDB to record employee information
   - The official Docker image is friendly and supports the initialization of the build table during the build phase.
   - Performance is fully sufficient to support current business scenarios.
   - It has strong scalability. Even in actual scenarios, distributed deployment and capacity expansion can be quickly achieved after business expansion. And there is no upgrade burden on the code.
## Storage design
Based on the above architectural principles and requirements, the storage only relies on Myqsl as RDB.

### Table Structure
- employee_info Table  <br/>
``` sql
CREATE TABLE `employee_info` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `created_time` timestamp NOT NULL COMMENT 'created time',
  `updated_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'updated time',
  `company_name` varchar(255) NOT NULL COMMENT 'employee''s company',
  `employee_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'employee name',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT 'email',
  `salary` int(10) unsigned zerofill NOT NULL COMMENT 'salry',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_cpn_employee_email` (`company_name`,`employee_name`),
  KEY `idx_cmny_name_salary` (`company_name`,`salary`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;
```
- user_info <br/>
User table (not required by this requirement, if it is more complete with login and API authentication, this table will be needed. Currently only token is recorded and used as API interface sign verification mechanism)
``` sql
CREATE TABLE `user_info` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `created_time` timestamp NOT NULL,
  `updated_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;
```

## API Protocol
The interface provides services through the HTTP protocol, and all interfaces need to pass sign for authentication. <br/>
sign sign algorithm rules:
``` php
$token = 'tmp_token'; // Just for test,Not have User Login yet. use fixed value: tmp_token
$params = array(
    'key'=>'value',
);
ksort($params);
foreach($params as $key => $value){
    if($key != 'sign'){
        $tmpSign .= $value;
    }
}
$tmpSign .= $token;
$sign = md5($tmpSign);
```

- UploadCsv
    - Desc <br/>
        For uploading employee's csv file, if do repeat upload will update the salary and email infos.
    - Path:
        /api/upload
    - Content-type:
         multipart/form-data
    - Method:
        POST
    - Host: https://richardw.site
    - Params

    | name | type | required | sample
    | :----:| :----: | :----: | :----:
    | sign | string | true | 3ba18ed945bf272807f212f6ffbb9970 |
    | employee_infos_csv | file | false | upload.scv|
    - Response
    ``` json
    {
        "error_no": 0,
        "error_message": "success ",
        "data": {
            "add_cnt": 0 // sucessfully insert recored num
        }
    }

    ```
- Modify Email
    - Desc <br/>
        For modifing employee's email by ID.
    - Path:
        /api/modifyemail
    - Content-type:
         application/x-www-form-urlencoded
    - Method:
        POST
    - Host:
    https://richardw.site
    - Params

    | name | type | required | sample
    | :----:| :----: | :----: | :----:
    | sign | string | true | 3ba18ed945bf272807f212f6ffbb9970 |
    | id | int | true | 1|
    | new_email | string | true | test@email.com |
    - Response
    ``` json
    {
        "error_no": 0,
        "error_message": "success ",
        "data": {
            "updated_cnt": 0 // sucessfully updated recored num
        }
    }

    ```
- Get employee info list
    - Desc <br/>
        For getting emoloyee info list.
    - Path:
        /api/getemployeelist
    - Method:
        GET
    - Host:
    https://richardw.site
    - Params

    | name | type | required | sample
    | :----:| :----: | :----: | :----:
    | sign | string | true | 3ba18ed945bf272807f212f6ffbb9970 |
    - Response
    ``` json
    {
        "error_no": 0,
        "error_message": "success ",
        "data": [
            {
                "id": 1,
                "company_name": "ACME Corporation",
                "employee_name": "John Doe",
                "email": "johndoe1@acme.com",
                "salary": "80000"
            },
            {
                "id": 2,
                "company_name": "ACME Corporation",
                "employee_name": "Jane Doe",
                "email": "janedoe@acme.com",
                "salary": "55000"
            }
        ]
    }

    ```
- Get campany's avg salary
- Desc <br/>
     For Geting campany's avg salary
    - Path:
        /api/getaveragesalary
    - Method:
        GET
    - Host:
    https://richardw.site
    - Params

    | name | type | required | sample
    | :----:| :----: | :----: | :----:
    | sign | string | true | 3ba18ed945bf272807f212f6ffbb9970 |
    - Response
    ``` json
    {
    "error_no": 0,
    "error_message": "success ",
    "data": [
        {
            "company_name": "ACME Corporation",
            "avg_salary": "57500.0000"
        },
        {
            "company_name": "Stark Industries",
            "avg_salary": "78750.0000"
        },
        {
            "company_name": "Wayne Enterprises",
            "avg_salary": "228000.0000"
        }
    ]
}

## Code dir structure
``` shell
.
├── Dockerfile
├── README.md
├── compose.yml
├── conf # some conf files for dockerfile building images.
│   ├── php.ini
│   ├── vhost.conf
│   └── www.conf
├── mysql # mysql images from mysql:latest, and add init.sql to create DB and tables.
│   ├── Dockerfile
│   ├── init.sh
│   └── init.sql
├── php-app # php dir, and this dir also be mounted to nginx cgi script dir at docker build stage.
│   ├── application
│   │   ├── Bootstrap.php
│   │   ├── actions # action layer, each file will be loaded accroding to controllers' define.
│   │   │   ├── GetAverageSalary.php
│   │   │   ├── GetEmployeeList.php
│   │   │   ├── ModifyEmail.php
│   │   │   └── Upload.php
│   │   ├── controllers # controller layer, mianly for routes dispatching to specific action.
│   │   │   └── Api.php
│   │   ├── library # libs dir, all the lib files define here and will be autoloaded.
│   │   │   ├── ErrorCode.php
│   │   │   ├── Log.php
│   │   │   ├── MException.php
│   │   │   ├── Render
│   │   │   │   └── Format.php
│   │   │   └── Utils.php
│   │   └── models # model layer, mainly for business logic implements
│   │       ├── Dao # data access obj layer, RDB, noRDB and so on all can do here. In principle, it is only allowed to be called by the Service layer.  
│   │       │   ├── Base.php # singleton DB base class, all specific logic dao extends this file.
│   │       │   ├── EmployeeInfo.php
│   │       │   └── UserInfo.php
│   │       └── Services # services layer, core business logic write here(call data from dao then pack and set to ret, In principle, only can be called form action layer).
│   │           ├── BaseService.php  # abstract base service class, define the core process only expose execute and getRet to caller.
│   │           ├── GetAvarageSalaryService.php
│   │           ├── GetemployeeListService.php
│   │           ├── ModifyEmailService.php
│   │           └── UploadCsvService.php
│   ├── conf
│   │   └── application.ini # conf defines, such as DB connection info params check rulers
│   └── index.php # main entrance
├── react-app # fe src dir, Based on the third UI component antd.
│   ├── README.md
│   ├── index.html
│   ├── package.json
│   ├── public
│   ├── src
│   │   ├── App.css
│   │   ├── App.jsx # core logic
│   │   ├── assets
│   │   │   └── react.svg
│   │   ├── components # for components
│   │   │   └── Uploader
│   │   │       ├── index.jsx
│   │   │       └── index.module.css
│   │   ├── index.css
│   │   ├── index.module.css
│   │   ├── main.jsx
│   │   └── vite-env.d.ts
│   ├── tsconfig.json
│   ├── tsconfig.node.json
│   ├── vite.config.js
│   └── yarn.lock
└── start.sh
```

## Discussion about the ideal solution
It's really a open topic, and there is a lot to be said for the ideal solution discussion. Choose to discuss multiple key topics such as product, architecture, deployment, security, service governance, etc.


### Product optimization
There are many things that can be done on the product. The following are some product suggestions mainly related to technical feasibility and safety.
- Add account system and login authentication <br/>

The system does not have user login and permission verification, which is very inconvenient in terms of security and data management. If multiple users with different roles try to use the system, it will be unusable.

- Added paging query to employee information list <br/>

There is no paging function when there is a large amount of employee data. It is not OK in terms of user experience, data reading performance and feasibility.

- While retaining csv import, add form modification function. After uploading the csv, the user first checks the data in the browser, and then submits it in form-data format if there is no problem. Rather than throwing the file directly to the API <br/>

Compared with file upload, the form verification link can increase accuracy. At the same time, avoid security risks caused by the file upload function.

### Architecture upgrade
- Split the single deployment <br/>
    Split the PHP back-end service and JS front-end APP, and deploy them as independent images to different business clusters or instances that are completely isolated. It is very helpful in terms of system stability, fault tolerance and scalability.
- Add multi-machine load balancing to API and front-end after splitting <br/>

   Adopt multi-instance deployment. If the business expands, consider multiple regions and multiple computer rooms. Can greatly improve availability and disaster recovery capabilities.

- Using distributed Msql database <br/>
    Adopt one master and multiple slaves or multiple master and multiple slave database deployment methods. Separation of reading and writing through writing master and reading slave

- -Introduce other storage <br/>
    If the project continues to iterate, the data magnitude and query condition complexity become very high. Relying only on Mysql performance and service stability will face challenges. Other storages, such as ES, can be introduced appropriately. Ensure stable writing while supporting more complex queries.

###  Prevent web security risks
- Risks of file upload <br/>
    Both the front-end and the back-end need to strictly filter and verify the true file type, file content, and file size. If there are loopholes, there will be great security risks.

-  Prevent CSRF vulnerabilities. <br/>
   Based on the account system, add tokens to front-end requests to prevent CSRF vulnerabilities.

- Filter and verify input content to prevent XSS vulnerabilities. <br/>
    Filter and check the content of any input entry. Every step must be strictly checked when the front-end inputs and after the back-end receives the data.

###  System disaster recovery

- Clarify core functions <br/>
    Prioritize the functions of the entire system and determine which functions are the core business at the P0 level as the guarantee target for disaster recovery. For example, for this employee management system, ensuring that employee information is uploaded and written is the P0 level.
- Added downgrade function <br/>
   Add the ability to downgrade services that are not P0 and can be restored later. For example, for this project, when system availability is reduced for some reasons, the read service can be downgraded, leaving only the ability to upload and write employee information.
- Back up core data
    Back up core business data regularly to avoid data loss.

###  Service Governance
- Key information logging <br/>
  Add logs to the core logic and joint nodes of the code. In order to monitor service status based on key information and detect system problems immediately

- Add monitoring and alarm <br/>
  Based on logs and other system information, monitor error information and promptly notify relevant personnel via email or phone call

- Add stream scheduling capabilitie <br/>
    Implementing traffic switching, in the event of unavailability of services in a specific data center or region. Instantly switching to another region or data center.

### CI/CD
- Staged Deployment <br/>

    On the basis of deploying in multiple data centers, the system's code release and deployment adopt a phased deployment approach with small traffic to minimize the impact of abnormal releases.

- Automated Testing <br/>

    On the basis of staged Deployment, the introduction of automated testing is implemented. In the small traffic phase, abnormal conditions are detected by monitoring the results of interfaces or page responses.

### Testing

- Unit Testing <br/>
  Code development is coupled with unit testing to ensure the quality of development. For example, using PHPUnit, and monitoring the test coverage during the CI/CD phase. If the coverage is below expectations, prompts or interception of the release can be implemented.

- AB Test <br/>
  AB testing can be employed for small-scale experiments on new features, ensuring stability online while evaluating the effectiveness of the new functionality.

----
# Personal Thoughts and Experiences

## Thoughts on the interview

Before coming to New Zealand, I have also participated in many technical interviews during more than 8 years of working in Beijing, China. I also interviewed other people many times. As far as my personal experience is concerned, this interview still had new and different experiences.


- The format of the coding exam is very novel and more pragmatic. <br/>
  Develop a complete project in a manner very similar to actual work development. It can give a good and comprehensive view of the candidate's coding ability and personal thinking, which is more open-minded than examining some fixed concepts or writing algorithm questions.

- Reasonable setting of examination questions. <br/>
   The requirements are not complex, but the inspection points are comprehensive. Including file processing, database CURD operations, and front-end page implementation. It also leaves enough space for candidates to show their thinking.

- The overall interview process is very friendly。



## Project Review

As an interview test question, the project took almost two weeks from when I received the question on February 7th to when I submitted it today, which is a bit long. Although there are some uncontrollable objective reasons that led to the failure to intervene in the development in time, I still feel very sorry for the people in my interview loop and the whole thing.

The following is a review of the time-consuming and manpower distribution based on gitlab historical records:
- 2.15-2.16 <br/>
Start coding
Complete image configuration
Complete the development of the PHP project code structure and base classes
Complete most of the PHP business logic.

- 2.16-2.17 <br/>
Complete php and FE function development
Modify the image to support fe code packaging and deployment
Debugging and bug fixes.

- 2.18-2.19 <br/>
Deploy to personal online cloud host
README manual writing

Problems encountered

- When packaging front-end code, the yarn command keeps failing during docker build. This is temporarily solved by replacing it with npm packaging (which is much slower than yarn). But the yarn build failed and needs to be further studied
