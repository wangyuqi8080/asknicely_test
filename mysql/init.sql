CREATE DATABASE IF NOT EXISTS `ask_nicely`;

USE `ask_nicely`;

DROP TABLE IF EXISTS employee_info;

CREATE TABLE `employee_info` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `created_time` timestamp NOT NULL COMMENT 'created time',
  `updated_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'updated time',
  `company_name` varchar(255) NOT NULL COMMENT 'employee''s company',
  `employee_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'employee name',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT 'email',
  `salary` int(10) unsigned zerofill NOT NULL COMMENT 'salry',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_cpn_employee_email` (`company_name`,`employee_name`),
  KEY `idx_cmny_name_salary` (`company_name`,`salary`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3; 

DROP TABLE IF EXISTS user_info;

CREATE TABLE `user_info` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `created_time` timestamp NOT NULL,
  `updated_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;

-- just for test, in real will use a fully register feature to do this
insert into `user_info`(`user_name`, `access_token`, `created_time`, `updated_time`) values('asknicely_test', 'tmp_token', now(), now());