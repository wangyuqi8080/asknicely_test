<?php
/**
 * @desc Bootstrap is a mechanism used to do some initial config, can do any init work here
 * with "_init{xx}" prefix function name will be loaded in order.
 */
class Bootstrap extends Yaf_Bootstrap_Abstract 
{
    public function _initRoute(Yaf_Dispatcher $dispatcher) {}
    
    
}