<?php

/**
 * @desc action for modify upload
 * @athor richard
 * @date 2024/02/15
 */
class GetEmployeeListAction extends Yaf_Action_Abstract {
    public function execute(){
        // get service instance
        $serviceUpload = new Services_GetemployeeListServiceModel();
        // service execute
        $serviceUpload->execute();
        $ret = $serviceUpload->getRet();
        
        // render rest
        if(is_array($ret)){
            Render_Format::JsonOutput(ErrorCode::CODE_SUCESS, $ret, '');
        }else{
            throw new Exception(ErrorCode::errorMessage[ErrorCode::CODE_ERROR_SERVICE_FAIL], ErrorCode::CODE_ERROR_SERVICE_FAIL);
        }
    }
}