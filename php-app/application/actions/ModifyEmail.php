<?php

/**
 * @desc action for modify email
 * @athor richard
 * @date 2024/02/15
 */
class ModifyEmailAction extends Yaf_Action_Abstract {
    public function execute(){
        // get service instance
        $serviceModifyEmail = new Services_ModifyEmailServiceModel();
        
        // service execute
        $serviceModifyEmail->execute();
        $ret = $serviceModifyEmail->getRet();
        
        // render rest
        if(is_array($ret)){
            Render_Format::JsonOutput(ErrorCode::CODE_SUCESS, $ret, '');
        }else{
            throw new Exception(ErrorCode::errorMessage[ErrorCode::CODE_ERROR_SERVICE_FAIL], ErrorCode::CODE_ERROR_SERVICE_FAIL);
        }
    }
}