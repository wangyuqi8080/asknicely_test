<?php

/**
 * @desc action for csv upload
 * @athor richard
 * @date 2024/02/15
 */
class UploadAction extends Yaf_Action_Abstract {
    public function execute(){

        // get service instance
        $serviceUpload = new Services_UploadCsvServiceModel();
        
        // service execute
        $serviceUpload->execute();
        $ret = $serviceUpload->getRet();
        
        // render rest
        if(is_array($ret)){
            Render_Format::JsonOutput(ErrorCode::CODE_SUCESS, $ret, '');
        }else{
            throw new Exception(ErrorCode::errorMessage[ErrorCode::CODE_ERROR_SERVICE_FAIL], ErrorCode::CODE_ERROR_SERVICE_FAIL);
        }
    }
}