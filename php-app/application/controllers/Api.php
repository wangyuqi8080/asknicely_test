<?php

/**
 * @desc Api controller, all the actions map will be 
 * declared here.
 * @author richard
 * @date 2024/01/15
 */
class ApiController extends Yaf_Controller_Abstract {

    /**
     * @desc Must disable the view template render, 
     * or will pose a error cause of no template file.
     */
    public function init() {
        Yaf_Dispatcher::getInstance()->disableView();
    }

    /**
     * @desc Register actions at $actions array, and the 
     * dispatcher will load the specific action accroding 
     * to the routers' rest
     *  */ 
    public $actions = array(
        "upload" => "actions/Upload.php",
        "getemployeelist" => "actions/GetEmployeeList.php",
        "modifyemail" => "actions/ModifyEmail.php",
        "getaveragesalary" => "actions/GetAverageSalary.php",
    );
}