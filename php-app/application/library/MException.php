<?php

/**
 * @desc Customerlized Exception class extend from internal php Exception,
 * can do more such as converge the error log printing, add error metric.
 */
class MException extends Exception {
    /**
     * @desc do more  
     */
    public function __construct($errstr, $err_no) {
        // do more here..

        parent::__construct($errstr, $err_no);
    }
}