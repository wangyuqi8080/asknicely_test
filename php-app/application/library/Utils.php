<?php

class Utils {

    /**
     * @desc params sign check, if the sign from request 
     * not equal the hash rst will return false, otherwise 
     * true.
     * @param $sign string
     * @param $params array
     * @param $token string
     * @return bool
     */
    public static function signCheck($params, $token){
        return true; // for debug
        $ret = false;
        $tmpSign = '';
        if(isset($params['sign']) && is_array($params) && !empty($params['sign']) && !empty($token)){
            ksort($params);
            foreach($params as $key => $value){
                if($key != 'sign'){
                    $tmpSign .= $value;
                }
            }
            $tmpSign .= $token;
            $tmpSign = md5($tmpSign);
            if($tmpSign == $params['sign']){
                $ret = true;
            }
        } 
        return $ret;
    }

    /**
     * @desc params check by rulers
     * @param $params array()
     * @param $method string
     * @return mixed
     */
    public static function checkParamsByRulers($params, $method, $page){
        $ret = array(
            'pass'=>true,
            'error_info'=>'',
        );
        $config = Yaf_Application::app()->getConfig()->toArray();
        $rulers = $config['param_ruler'][$page];
        if(is_array($rulers) && count($rulers) > 0 && in_array($method, array('post','get'))){
            foreach($rulers[$method] as $key=>$ruler){
                if($ruler['required'] == "1" && !isset($params[$key])){
                    $ret['pass'] = false;
                    $ret['error_info'] = "missing required param $key";
                    break;
                }
                switch($ruler['type']){
                    case 'int':
                        $ret = !is_numeric($params[$key]) 
                        ? array('pass'=>false, 'error_info'=>"$key type error must be int") 
                        : $ret;
                        break;
                    case 'str':
                        $ret = !is_string($params[$key]) 
                        ? array('pass'=>false, 'error_info'=>"$key type error must be str") 
                        : $ret;
                        break;
                    default:
                        $ret = $ret;
                }
            }
        }

        return $ret;
    }
    
    /**
     * @desc convert pdo ret to key=value map
     * @param $pdoArr array
     * @return array
     */
    public static function convertPdoRet($pdoArr){
        $ret = array();

        if(count($pdoArr) > 0 && is_array($pdoArr)){
            foreach($pdoArr as $value){
                $recordTmpArr = array();
                foreach($value as $key=>$columnValue){
                    if(is_string($key)){
                        if(in_array(strtolower($key), array('salary'))){
                            // num convert to string num, in case of the large num's loss of precision at FE(js double max only 52 bits)
                            $recordTmpArr[strtolower($key)] = strval(intval($columnValue));
                        }else{  
                            $recordTmpArr[strtolower($key)] = $columnValue;
                        }
                    }
                }
                $ret[] = $recordTmpArr;
            }
        }

        return $ret;
    }
}