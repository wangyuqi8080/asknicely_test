<?php

/**
 * @desc For employee Info logic
 */
class Dao_EmployeeInfoModel extends Dao_BaseModel {

    const DEFAULT_INT = 0;
    const DEFAULT_STR = '';

    private static $_columnFileds = array(
        'created_time',
        'updated_time',
        'company_name',
        'employee_name',
        'email',
        'salary',
    );

    private static $_columnFiledsType = array(
        'created_time' => PDO::PARAM_STR,
        'updated_time' => PDO::PARAM_STR,
        'company_name' => PDO::PARAM_STR,
        'employee_name' => PDO::PARAM_STR,
        'email' => PDO::PARAM_STR,
        'salary'=> PDO::PARAM_INT,
    );

    /**
     * @desc can set different DB config for different logic
     * @param void
     * @return void
     */
    public static function setConfig(){
        /*$config = Yaf_Application::app()->getConfig()->toArray();
                self::$_config = (isset($config['mysql']) && !empty($config['mysql']))
                ? $config['mysql']
                : array();
        self::$_config = $config['mysql'];*/
    }

    /**
     * @desc set table name
     * @param $table string
     * @return void
     */
    public static function setTable($table){
        self::$_table = $table;
    }

    /**
    * @desc get EmployeeList
    * @param viod
    * @return array
    */
    public static function getEmployeeList(){
        $ret = array();
        try{
            $sql = sprintf("select id, company_name, employee_name, email, salary from %s", self::$_table);
            $ret = self::getAll($sql);
        }catch(PDOException $e){
            throw new Exception($e->getMessage(), ErrorCode::CODE_DB_ERROR);
        }
        return $ret;
    }

    /**
    * @desc get AvarageSalary
    * @param viod
    * @return array
    */
    public static function getAvarageSalary(){
        $ret = array();
        try{
            $sql = "select company_name, sum(a.salary)/count(id) as avg_salary from %s as a group by company_name;";
            $sql = sprintf($sql, self::$_table);
            $ret = self::getAll($sql);
        }catch(PDOException $e){
            throw new Exception($e->getMessage(), ErrorCode::CODE_DB_ERROR);
        }
        return $ret;
    }

    /**
     * @desc Insert emloyeeInfos
     * @param $infos array(
     *                      array(
     *                          'column_name'=>'column_value',
     *                          ....
     *                      ),
     *                      ....
     *                     )
     * @return int 
     */
    public static function addEmployeeInfos($infos){
        $ret = 0;
        $instance = self::getInstance();
        if(is_array($infos) && !empty($infos)){
            $recordNum = count($infos);
            $eachValuePlaceHolder = implode(',', array_fill(0, count(self::$_columnFileds), '?'));
            $placeHolder = implode(',', array_fill(0, $recordNum, "($eachValuePlaceHolder)"));
            $fileds = '';
            foreach(self::$_columnFileds as $column){
                $fileds .= "`$column`,";
            }
            $fileds = substr($fileds, 0, -1);
            $insertSql = "insert into %s(%s) values %s on duplicate key update `salary`=values(`salary`), `email`=values(`email`)";
            $insertSql = sprintf($insertSql, self::$_table, $fileds, $placeHolder);
            $stateMent = $instance->prepare($insertSql);
            $i = 1;
            foreach($infos as $eachRecord){
                if (count($eachRecord) == count(self::$_columnFileds)){
                    foreach(self::$_columnFileds as $column){
                        if(isset($eachRecord[$column])){
                            $stateMent->bindParam($i, $eachRecord[$column], self::$_columnFiledsType[$column]);
                        }else{
                            // if not insert use empty value based on it's datatype to fulfill
                            switch (self::$_columnFiledsType[$column]) {
                                case PDO::PARAM_STR:
                                    $stateMent->bindParam($i, self::DEFAULT_STR, PDO::PARAM_STR);
                                    break;
                                case PDO::PARAM_INT:
                                    $stateMent->bindParam($i, self::DEFAULT_INT, PDO::PARAM_INT);
                                    break;
                            }
                        }
                        $i++;
                    }
                }else{
                    throw new Exception('record missided some colums, please check', ErrorCode::CODE_DB_ERROR);
                    break;
                }   
            }

            try{
                $stateMent->execute();
                $ret = $stateMent->rowCount();
            }catch(PDOException $e){
                throw new Exception($e->getMessage(), ErrorCode::CODE_DB_ERROR);
            }
        }
        return $ret;
    }

    /**
     * @desc Update employee's info by id
     * @param $id int
     * @param $updateInfos array(culumn_name=>update_value,...)
     * @return int
     */
    public static function updateEmloyeeInfos($id, $updateInfos){
        $ret = 0;
        $instance = self::getInstance();
        $updatePlaceHolder = '';
        $updateSql = '';
        if ($id > 0 && count($updateInfos) > 0){
            foreach($updateInfos as $column=>$value){
                if (in_array($column, self::$_columnFileds)) {
                    switch (self::$_columnFiledsType[$column]) {
                        case PDO::PARAM_STR:
                            $updatePlaceHolder .= " `$column`= '$value' and";
                            break;
                        case PDO::PARAM_INT:
                            $updatePlaceHolder .= " `$column`= $value and";
                            break;
                    }
                }
            }
            $updatePlaceHolder = substr($updatePlaceHolder, 0, -4);
            if(strlen($updatePlaceHolder) > 0) {
                $updateSql = sprintf("update %s set %s where id=%d", self::$_table, $updatePlaceHolder, intval($id));
            }else{
                throw new Exception('update column fail, please check!', ErrorCode::CODE_DB_ERROR);
            }

            try{
                $stateMent = $instance->prepare($updateSql);
                $stateMent->execute();
                $ret = $stateMent->rowCount();
            }catch(PDOException $e){
                throw new Exception($e->getMessage(), ErrorCode::CODE_DB_ERROR);
            }
        }

        return $ret;
    }
}