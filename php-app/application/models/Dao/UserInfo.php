<?php

/**
 * @desc For user Info logic
 */
class Dao_UserInfoModel extends Dao_BaseModel {

    /**
     * @desc can set different DB config for different logic
     * @param void
     * @return void
     */
    public static function setConfig(){
        // don't need to set, can use defualt DB config
    }

    /**
     * @desc set table name
     * @param $table string
     * @return void
     */
    public static function setTable($table){
        self::$_table = $table;
    }

    /**
    * @desc get UserInfo
    * @param viod
    * @return array
    */
    public static function getUserInfo(){
        $ret = array();
        try{
            $sql = sprintf("select id, user_name, access_token from %s", self::$_table);
            $ret = self::getAll($sql);
            $ret = isset($ret[0]) ? $ret[0] : array();
        }catch(PDOException $e){
            throw new Exception($e->getMessage(), ErrorCode::CODE_DB_ERROR);
        }
        return $ret;
    }
}