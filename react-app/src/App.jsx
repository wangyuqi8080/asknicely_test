import Uploader from './components/Uploader';
import { Tabs, Table, Space, Modal, Input, message } from 'antd';
import {useCallback, useEffect, useState} from 'react';
import css from './index.module.css';

import './App.css'

const TabPane = Tabs.TabPane;

const COLUMNS_COMPANY = [
  {
    title: 'Company Name',
    dataIndex: 'company_name',
    key: 'company_name',
  },
  {
    title: 'Average Salary',
    dataIndex: 'avg_salary',
    key: 'avg_salary',
    render: (text) => {
      return (+text).toFixed(2)
    }
  }
]

const getColumns = (handleEdit) => {
  return [
    {
      title: 'Company Name',
      dataIndex: 'company_name',
      key: 'company_name',
    },
    {
      title: 'Employee Name',
      dataIndex: 'employee_name',
      key: 'employee_name',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Salary',
      dataIndex: 'salary',
      key: 'salary',
    },
    {
      title: 'action',
      dataIndex: 'action',
      key: 'action',
      render: (text, record) => {
        return (
          <Space size="middle">
            <a onClick={() => {handleEdit(record)}}>Edit</a>
          </Space>
        )
      }
    }
  ];
}



function App() {
  const [tab, setTab] = useState('1');
  const [employList, setEmployList] = useState([]);
  const [averageSalaryList, setAverageSalaryList] = useState([]);
  const [loading, setLoading] = useState(false);

  const [editRecord, setEditRecord] = useState();
  const [editEmail, setEditEmail] = useState('');

  const getEmployeeList = async () => {
    try {
      setLoading(true);
      const response = await fetch('/api/getemployeelist');
      const data = await response.json();
      if (data?.error_no === 0) {
        setEmployList(data?.data || []);
      }
      setLoading(false);
    } catch (error) {
      message.error('api error');
      setLoading(false);
    }
  };

  const getAverageSalary = async () => {
    try {
      const response = await fetch('/api/getaveragesalary');
      const data = await response.json();
      if (data?.error_no === 0) {
        setAverageSalaryList(data?.data || []);
      }
    } catch (error) {
      message.error('api error');
    }
  }


  const updateEmail = async () => {
    try {
      if(!editEmail || !editEmail.trim()) {
        message.error('field should not be empty');
        return;
      }
      const formData = new URLSearchParams();
      formData.append('id', editRecord.id);
      formData.append('new_email', editEmail);
      await fetch('/api/modifyemail', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: formData
      });
      getEmployeeList();
      handleEdit({});
    } catch (error) {
      message.error('api error');
    }
  }

  const handleEdit = (record) => {
    setEditRecord(record);
    setEditEmail(record?.email);
  }

  const getData = useCallback(
    () => {
      getEmployeeList();
      getAverageSalary();
    }, []
  );

  useEffect(
    () => {
      getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []
  );

  return (
      <div className={css.wrap}>
        
        <h2>Employee Information</h2>
        <Uploader onSuccess={getData}/>

        <div className={css.tableWrap}>
          <Tabs defaultActiveKey="1" onChange={(tab) => setTab(tab)} activeKey={tab} >;
            <TabPane tab="Employee Info" key="1">
              <Table
                columns={getColumns(handleEdit)}
                dataSource={employList}
                rowKey="id"
                pagination={false}
                loading={loading}
              />
            </TabPane>
            <TabPane tab="Company Average Salary" key="2">
                <Table
                  columns={COLUMNS_COMPANY}
                  dataSource={averageSalaryList}
                  rowKey="company_name"
                  pagination={false}
                />
            </TabPane>
          </Tabs>

          
        </div>
        
        {
          editRecord?.id && (
            <Modal
              open
              title="Edit Email"
              onCancel={() => handleEdit({})}
              onOk={updateEmail}
            >
              <Input value={editEmail} onChange={(e) => setEditEmail(e.target.value)} />
            </Modal>
          )
        }
      </div>
  )
}

export default App
