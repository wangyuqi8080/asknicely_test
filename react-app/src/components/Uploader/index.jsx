import React from 'react';
import { UploadOutlined } from '@ant-design/icons';
import { Button, message, Upload } from 'antd';
import css from './index.module.css';

const Uploader = ({onSuccess}) => {
  const props = {
    name: 'employee_infos_csv',
    action: '/api/upload',
    accept: '*.csv',
    headers: {
      authorization: 'authorization-text',
    },
    showUploadList: false,
    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
        onSuccess && onSuccess();
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };
  return (
    <div className={css.uploadWrap}>
      <Upload {...props}>
        <Button icon={<UploadOutlined />}>Click to Upload CSV File</Button>
      </Upload>
    </div>
    
  )
};
export default Uploader;